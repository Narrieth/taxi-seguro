// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAANPZ4TrURFy1RH7fissBwcr1SuXC9hlA",
    authDomain: "taxi-seguro-c7b74.firebaseapp.com",
    databaseURL: "https://taxi-seguro-c7b74.firebaseio.com",
    projectId: "taxi-seguro-c7b74",
    storageBucket: "taxi-seguro-c7b74.appspot.com",
    messagingSenderId: "441195022133",
    appId: "1:441195022133:web:38857df00b5b33b4feee7c",
    measurementId: "G-WP226W6V6X"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
