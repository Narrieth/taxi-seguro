import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { resolve } from 'url';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  afs: any;
  unsubscribeOnLogOut() {
    throw new Error("Method not implemented.");
  }

  constructor() { }

  createTask(value) {
    return new Promise<any>((resolve, reject) => {
      let currentUser = firebase.auth().currentUser;
      this.afs.collection('people').doc(currentUser.uid)
        .collection('task').add({
          title: value.title,
          description: value.description,
          image: value.image
        })
        .then(
          res => resolve(res),
          err => reject(err)
        )
    })
  }
}
